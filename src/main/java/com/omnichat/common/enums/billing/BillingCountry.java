package com.omnichat.common.enums.billing;

import static java.util.stream.Collectors.toMap;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

/** BillingCountry enumerations; */
public enum BillingCountry {
  TAIWAN("tw", new BigDecimal("0.005")),
  HONG_KONG("hk", BigDecimal.ZERO),
  MALAYSIA("my", BigDecimal.ZERO);

  private static final Map<String, BillingCountry> stringToEnumMap =
      Arrays.stream(values()).collect(toMap(Object::toString, Function.identity()));

  private final String abbreviation;
  private final BigDecimal taxRate;

  BillingCountry(String abbreviation, BigDecimal taxRate) {
    this.abbreviation = abbreviation;
    this.taxRate = taxRate;
  }

  public static Optional<BillingCountry> fromAbbreviation(String abbreviation) {
    return Optional.ofNullable(stringToEnumMap.get(abbreviation.toLowerCase()));
  }

  public String getAbbreviation() {
    return abbreviation;
  }

  public BigDecimal getTaxRate() {
    return taxRate;
  }

  @Override
  public String toString() {
    return abbreviation;
  }
}

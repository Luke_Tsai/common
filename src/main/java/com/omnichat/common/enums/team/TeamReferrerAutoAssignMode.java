package com.omnichat.common.enums.team;

/**
 * Referrer auto binding mode random = binding to sales user randomly manager = binding to manager
 */
public class TeamReferrerAutoAssignMode {
  public static final int RANDOM = 1;
  public static final int MANAGER = 2;
}

package com.omnichat.common.enums.team;

public class TeamProductionFeedFormat {

  public static final String CSV = "csv";
  public static final String XML = "xml";
}

package com.omnichat.common.enums.billing;

import static java.util.stream.Collectors.toMap;

import com.fasterxml.jackson.annotation.JsonValue;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

/** Features that can be charged from a team's balance */
public enum BalanceFeature {

  // Used for modifying a balance by admin user
  ADMIN_MODIFY(0),
  LINE_PNP(1),
  RESNET_PNP_WITH_SMS(2),
  LIVE_VIDEO(3);

  private static final Map<Integer, BalanceFeature> intToEnumMap =
      Arrays.stream(values()).collect(toMap(BalanceFeature::getValue, e -> e));

  private final int value;

  BalanceFeature(int value) {
    this.value = value;
  }

  public static Optional<BalanceFeature> fromValue(int value) {
    return Optional.ofNullable(intToEnumMap.get(value));
  }

  @JsonValue
  public int getValue() {
    return value;
  }
}

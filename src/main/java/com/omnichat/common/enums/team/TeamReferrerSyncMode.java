package com.omnichat.common.enums.team;

/**
 * REFERRER_SYNC_MODE_NON_SYNC (1):
 *
 * <p>- For non-91App OMO team, no need to call 91App bind referrer API;
 *
 * <p>- For 91App team but don't want call 91App bind referrer API to sync
 *
 * <p>REFERRER_SYNC_MODE_91APP_SYNC (2):
 *
 * <p>- For 91App team only - Will call 91app bind referrer API to get/add referrer binding from/to
 * 91App
 *
 * <p>- when receive 91app data sync, will update the referrer binding
 *
 * <p>REFERRER_SYNC_MODE_91APP_SYNC_NO_CREATE (3):
 *
 * <p>- For 91App team only - Will only check if any binding in 91App, if no binding, won't add to
 * 91App
 *
 * <p>- Call 91app check referrer binding API
 */
public class TeamReferrerSyncMode {

  public static final int NON_SYNC = 1;
  public static final int SYNC_91APP = 2;
  public static final int SYNC_91APP_NO_CREATE = 3;
}

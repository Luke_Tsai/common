package com.omnichat.common.dto.shop;

public record ShopLocationDto(String code, String name) {}

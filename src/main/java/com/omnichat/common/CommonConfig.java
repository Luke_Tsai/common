package com.omnichat.common;

import com.omnichat.common.utils.YamlPropertySourceFactory;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

@ComponentScan
@ConfigurationPropertiesScan
@PropertySource(
    value = {"classpath:common-config-${spring.profiles.active}.yml"},
    factory = YamlPropertySourceFactory.class)
public class CommonConfig {}

package com.omnichat.common.enums.team;

public class TeamMaxSubscriber {
  public static final int DEFAULT_SUBSCRIBERS_NUMBERS = 500;
  public static final int DEFAULT_MAX_AGENT = 10000;
  public static final int DEFAULT_NO_OF_AGENT = 2;
  public static final int DEFAULT_REMARKETING_IMPRESSION_NUMBERS = 5000;
}

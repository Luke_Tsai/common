package com.omnichat.common.enums.user;

import static java.util.stream.Collectors.toMap;

import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

public enum UserRole {
  UNDEFINED("UNDEFINED", -1),
  GUEST("GUEST", 0),
  ADMIN("ADMIN", 1),
  CS("CS", 2),
  MARKETING("MARKETING", 3),
  CS_MANAGER("CS_MANAGER", 4),
  MANAGER("MANAGER", 5),
  SALESPERSON("SALESPERSON", 6),
  SALES_MANAGER("SALES_MANAGER", 7),
  MARKETING_CS("MARKETING_CS", 8);

  private static final Map<String, UserRole> nameToEnumMap =
      Arrays.stream(values()).collect(toMap(UserRole::getName, Function.identity()));
  private static final Map<Integer, UserRole> codeToEnumMap =
      Arrays.stream(values()).collect(toMap(UserRole::getCode, Function.identity()));
  private final String name;
  private final int code;

  UserRole(String name, int code) {
    this.name = name;
    this.code = code;
  }

  public static Optional<UserRole> fromName(String name) {
    return Optional.ofNullable(nameToEnumMap.get(name));
  }

  public static Optional<UserRole> fromCode(Integer code) {
    return Optional.ofNullable(codeToEnumMap.get(code));
  }

  public String getName() {
    return name;
  }

  public int getCode() {
    return code;
  }
}

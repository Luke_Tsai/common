package com.omnichat.common.enums.product;

public class ProductCloud {
  public static final String FREE = "FREE-CLOUD-2021";
  public static final String CS = "CS-CLOUD-2021";
  public static final String MARKETING_GROWTH = "MARKETING-GROWTH-CLOUD-2021";
  public static final String MARKETING_ADVANCED = "MARKETING-ADVANCED-CLOUD-2021";
  public static final String OMO = "OMO-SALES-CLOUD-2021";
}

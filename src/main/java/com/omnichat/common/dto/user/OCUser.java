package com.omnichat.common.dto.user;

import com.omnichat.common.enums.user.UserRole;
import javax.annotation.Nullable;
import org.immutables.value.Value;

@Value.Immutable
public abstract class OCUser {

  @Value.Default
  public boolean getSSO() {
    return false;
  }

  public abstract String getUsername();

  @Nullable
  public abstract String getPassword();

  @Nullable
  public abstract String getTeam();

  public abstract String getEmail();

  public abstract Boolean getAdmin();

  public abstract Boolean getAnonymous();

  @Nullable
  public abstract String getProfilePath();

  @Nullable
  public abstract String getMethod();

  @Value.Default
  public int getRole() {
    return UserRole.UNDEFINED.getCode();
  }
}

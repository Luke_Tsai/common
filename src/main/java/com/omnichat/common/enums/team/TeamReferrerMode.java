package com.omnichat.common.enums.team;

/**
 * Referrer binding mode shop = bind to shop location employee = bind to sales user but not allow
 * overwrite overwrite = bind to sales user and allow overwrite
 */
public class TeamReferrerMode {

  public static final int SHOP = 1;
  public static final int EMPLOYEE = 2;
  public static final int OVERWRITE = 3;
}

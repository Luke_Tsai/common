package com.omnichat.common.enums.billing;

import static java.util.stream.Collectors.toMap;

import com.fasterxml.jackson.annotation.JsonValue;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

/** How to charge the team for features */
public enum BillingMode {
  PAY_IN_ADVANCE(0),
  PAY_IN_ARREARS(1);

  private static final Map<Integer, BillingMode> intToEnumMap =
      Arrays.stream(values()).collect(toMap(BillingMode::getValue, e -> e));

  private final int value;

  BillingMode(int value) {
    this.value = value;
  }

  public static Optional<BillingMode> fromValue(int value) {
    return Optional.ofNullable(intToEnumMap.get(value));
  }

  @JsonValue
  public int getValue() {
    return value;
  }
}

package com.omnichat.common.enums.team;

public class TeamStatus {

  public static int INVALIDATED = 0;
  public static int VALIDATED = 1;
  public static int SUPSPENSED = 2;
}

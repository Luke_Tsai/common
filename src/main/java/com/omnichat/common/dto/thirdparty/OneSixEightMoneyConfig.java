package com.omnichat.common.dto.thirdparty;

public record OneSixEightMoneyConfig(String domain, String key) {}

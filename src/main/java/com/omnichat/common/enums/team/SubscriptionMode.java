package com.omnichat.common.enums.team;

public class SubscriptionMode {
  public static final int BY_AGENT = 1;
  public static final int BY_SUBSCRIBERS = 2;
}

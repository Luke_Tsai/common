package com.omnichat.common.enums.team;

public class TeamTagLimit {
  public static final int MAX_TAG = 1000000;
  public static final int DEFAULT_MAX_TAG = 10;
}

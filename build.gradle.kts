plugins {
    id("org.springframework.boot") version "2.6.2" apply false
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
    id("java-library")
    id("com.diffplug.spotless") version "6.0.5"
}

group = "com.omnichat"
version = "0.0.1"
java.sourceCompatibility = JavaVersion.VERSION_17

repositories {
    mavenCentral()
}

dependencyManagement {
    imports {
        mavenBom(org.springframework.boot.gradle.plugin.SpringBootPlugin.BOM_COORDINATES)
    }
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter")

    api("org.apache.commons:commons-lang3:3.12.0")
    api("commons-io:commons-io:2.11.0")
    api("javax.mail:javax.mail-api:1.6.2")
    api("org.freemarker:freemarker:2.3.31")

    val jacksonVersion = "2.13.1"
    api("com.fasterxml.jackson.core:jackson-core:$jacksonVersion")
    api("com.fasterxml.jackson.core:jackson-annotations:$jacksonVersion")
    api("com.fasterxml.jackson.core:jackson-databind:$jacksonVersion")

    api("com.google.code.findbugs:jsr305:3.0.2")

    val immutablesVersion = "2.8.8"
    compileOnly("org.immutables:value:$immutablesVersion")
    annotationProcessor("org.immutables:value:$immutablesVersion")

    testImplementation("org.springframework.boot:spring-boot-starter-test")
}

tasks.withType<Test> {
    useJUnitPlatform()
}

// Use spotless to reformat files when building
tasks.getByName("build") {
    dependsOn("spotlessApply")
}

tasks.register("createDevConfig") {
    group = "devBuild"
    description =
        "Create a $name-config-dev.yml from $name-config-dev-template.yml. $name-config-dev.yml would be ignore by Git."

    doFirst {
        val name = project.name
        val resourceDir = "$projectDir/src/main/resources"
        val devProfile = file("$resourceDir/$name-config-dev.yml")
        if (!devProfile.exists()) {
            val devTemplate = file("$resourceDir/$name-config-dev-template.yml")
            copy {
                from(devTemplate)
                into(resourceDir)
                rename {
                    it.replace("$name-config-dev-template.yml", "$name-config-dev.yml")
                }
            }
            println("Create $name-config-dev.yml")
        } else {
            println("$name-config-dev.yml has existed")
        }
    }
}

// Since this library is included as a jar in our jib projects, we want the
// jar to built reproducibly.
tasks.withType<AbstractArchiveTask> {
    isPreserveFileTimestamps = false
    isReproducibleFileOrder = true
}

spotless {
    format("misc") {
        // define the files to apply `misc` to
        target("*.gradle", "*.md", ".gitignore")

        // define the steps to apply to those files
        trimTrailingWhitespace()
        indentWithSpaces() // or spaces. takes an integer argument if you don't like 4
        endWithNewline()
    }
    java {
        replaceRegex("Remove wildcard imports", "import\\s+[^\\*\\s]+\\*;(\\r\\n|\\r|\\n)", "$1")
        removeUnusedImports()
        // apply a specific flavor of google-java-format
        googleJavaFormat().reflowLongStrings()
    }
    kotlinGradle {
        target("*.gradle.kts") // default target for kotlinGradle
        ktlint() // or ktfmt() or prettier()
    }
}

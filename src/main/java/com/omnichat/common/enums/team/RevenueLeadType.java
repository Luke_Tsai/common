package com.omnichat.common.enums.team;

public class RevenueLeadType {

  public static final String BD = "BD";
  public static final String GROWTH = "Growth";
  public static final String SEMINAR = "Seminar";
  public static final String DIRECT_SALES = "Direct Sales";
}

package com.omnichat.common.enums.team;

/**
 * tx link revenue calculation mode `match`: only if the ocsaid match the assigned agent username in
 * the channel user is valid. If not match, the purchase belongs to no one. `link_first`: revenue
 * belongs the ocsaid in the tracking
 */
public class TeamTxLinkMode {

  public static final String MATCH = "match";
  public static final String LINK_FIRST = "link_first";
}

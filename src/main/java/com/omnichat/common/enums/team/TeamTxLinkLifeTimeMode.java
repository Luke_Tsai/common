package com.omnichat.common.enums.team;

/**
 * tx link lifetime calculation mode
 *
 * <p>`1`: the lifetime of a tx link is start from the click time `2`: the lifetime of a tx link is
 * start from the send time
 */
public class TeamTxLinkLifeTimeMode {

  public static final int CLICK = 1;
  public static final int SEND = 2;
}

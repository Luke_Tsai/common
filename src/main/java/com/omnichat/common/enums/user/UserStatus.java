package com.omnichat.common.enums.user;

import static java.util.stream.Collectors.toMap;

import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

public enum UserStatus {
  NONVALIDATED(0),
  VALIDATED(1),
  DEACTIVATED(2);

  private static final Map<Integer, UserStatus> statusToEnumMap =
      Arrays.stream(values()).collect(toMap(UserStatus::getCode, Function.identity()));
  private final int code;

  UserStatus(int code) {
    this.code = code;
  }

  public static Optional<UserStatus> fromCode(Integer code) {
    return Optional.ofNullable(statusToEnumMap.get(code));
  }

  public int getCode() {
    return code;
  }
}

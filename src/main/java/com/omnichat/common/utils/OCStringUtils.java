package com.omnichat.common.utils;

import org.apache.commons.lang3.StringUtils;

public class OCStringUtils {

  /**
   * Check if the input String is null, empty or equals to "null"
   *
   * @param str
   * @return
   */
  public static boolean isNullOrEmpty(String str) {
    return StringUtils.isBlank(str) || str.equalsIgnoreCase("null");
  }
}

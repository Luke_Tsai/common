package com.omnichat.common.enums.team;

public class SubscriptionType {
  public static final int TEAM = 1;
  public static final int ENTERPRISE = 2;
  public static final int CLOUD = 3;
}

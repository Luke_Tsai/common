package com.omnichat.common.enums.team;

public class SubscriberNumbers {
  public static int DEFAULT_NUMBERS = 500;
  public static int DEFAULT_MAX_AGENT = 10000;
  public static int DEFAULT_NO_OF_AGENT = 2;
  public static int DEFAULT_REMARKETING_IMPRESSION_NUMBERS = 5000;
}

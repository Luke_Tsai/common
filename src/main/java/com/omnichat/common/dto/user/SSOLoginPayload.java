package com.omnichat.common.dto.user;

public record SSOLoginPayload(String type, String payload, boolean userChanged) {}
